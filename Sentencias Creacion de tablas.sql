SENTENCIAS DE CREACIÓN DE TABLAS

CREATE TABLE actor
	(cedula int(10) not null,
	nombre VARCHAR(30) not null,
	apellido VARCHAR(30) not null,
	PRIMARY KEY (cedula))
ENGINE=InnoDB;


CREATE TABLE pelicula
	(codigo int(10) not null,
	nombre VARCHAR(30) not null,
	genero VARCHAR(30) not null,
	puntaje int(10) not null,
	idioma VARCHAR(30) not null,
	PRIMARY KEY (codigo))
ENGINE=InnoDB;

CREATE TABLE actuacion
	(actor int(10) not null,
	pelicula int(10) not null,
	personaje VARCHAR(30) not null,
	PRIMARY KEY (actor, pelicula, personaje),
	CONSTRAINT a_a FOREIGN KEY (actor) REFERENCES actor (cedula),
	CONSTRAINT a_p FOREIGN KEY (pelicula) REFERENCES pelicula (codigo))
ENGINE=InnoDB;

INSERT INTO pelicula(codigo, nombre, genero, puntaje, idioma) VALUES (1, "Rapido y Furioso", "accion", 6, "ingles")
INSERT INTO pelicula(codigo, nombre, genero, puntaje, idioma) VALUES (2, "Pulp Fiction", "accion", 10, "ingles")

INSERT INTO actor(cedula, nombre, apellido) VALUES (901,"Vin","Diesel")
INSERT INTO actor(cedula, nombre, apellido) VALUES (902,"Juan","Bustamante")
INSERT INTO actor(cedula, nombre, apellido) VALUES (903,"Jhonny","Deep")
INSERT INTO actor(cedula, nombre, apellido) VALUES (904,"Megan","Fox")




