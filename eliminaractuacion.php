<?php 

require('configs/include.php');

class c_eliminaractuacion extends super_controller {
    
	public function eliminar()
    {
	
		$actuacion = new actuacion($this->post);
		$this->orm->connect();
        $this->orm->delete_data("normal",$actuacion);
        $this->orm->close();
        
        $this->type_warning = "success";
        $this->msg_warning = "Actuacion eliminada correctamente";
        
        $this->temp_aux = 'message.tpl';
        $this->engine->assign('type_warning',$this->type_warning);
        $this->engine->assign('msg_warning',$this->msg_warning);
	
	}
	
    public function buscar()
    {
		$aux_actor = new actor($this->post);
		$cc['actor']['id']=$aux_actor->get('cedula');
		$options['actor']['lvl2'] = "by_id";
		$this->orm->connect();
		$this->orm->read_data(array("actor"), $options, $cc);
		$actor = $this->orm->get_objects("actor");
		$this->orm->close();
		
		if(is_empty($actor)){
			throw_exception("El actor no existe");
		}
		else{
		
		$this->engine->assign('BotonCedula',1);
		$this->engine->assign('actor',$actor[0]);		
		$cc['actuacion']['actor']=$aux_actor->get('cedula');
		$options['actuacion']['lvl2'] = "by_actor";
		$this->orm->connect();
		$this->orm->read_data(array("actuacion"), $options, $cc);
		$actuacion = $this->orm->get_objects("actuacion");
		$this->orm->close();
		
		$this->engine->assign('actuacion',$actuacion);
		
		}
    }

    public function display()
    {
        $this->engine->display('header.tpl');
        $this->engine->display($this->temp_aux);
        $this->engine->display('eliminaractuacion.tpl');
        $this->engine->display('footer.tpl');
    }
    
    public function run()
    {
        try {if (isset($this->get->option)){$this->{$this->get->option}();}}
        catch (Exception $e) 
		{
			$this->error=1; $this->msg_warning=$e->getMessage();
			$this->engine->assign('type_warning',$this->type_warning);
			$this->engine->assign('msg_warning',$this->msg_warning);
			$this->temp_aux = 'message.tpl';
		}    
        $this->display();
    }
}

$call = new c_eliminaractuacion();
$call->run();

?>
