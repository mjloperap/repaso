<?php 

require('configs/include.php');

class c_registraractuacion extends super_controller {
    
    public function add()
    {
        $actuacion = new actuacion($this->post);
		
        if(is_empty($actuacion->get('actor')) or is_empty($actuacion->get('pelicula')) or is_empty($actuacion->get('personaje'))){
			throw_exception("Existen campos vacios, debe completar todos los campos para continuar");
		}
		else{
			
			$cc['actor']['id']=$actuacion->get('actor');
			$options['actor']['lvl2'] = "by_id";
			$this->orm->connect();
			$this->orm->read_data(array("actor"), $options, $cc);
			$actor = $this->orm->get_objects("actor");
			$this->orm->close();
			
			if(is_empty($actor)){
				throw_exception("El actor no existe");
			}
			else{
			
				$cc['pelicula']['id']=$actuacion->get('pelicula');
				$options['pelicula']['lvl2'] = "by_id";
				$this->orm->connect();
				$this->orm->read_data(array("pelicula"), $options, $cc);
				$pelicula = $this->orm->get_objects("pelicula");
				$this->orm->close();
				
				if(is_empty($pelicula)){
				throw_exception("La pelicula no existe");
				}
				else{
				
					$this->orm->connect();
					$this->orm->insert_data("normal",$actuacion);
					$this->orm->close();					
				
				}
			}		
		}
		
		$this->type_warning = "success";
		$this->msg_warning = "actuacion agregada correctamente";
				
		$this->temp_aux = 'message.tpl';
		$this->engine->assign('type_warning',$this->type_warning);
		$this->engine->assign('msg_warning',$this->msg_warning);
		
    }

    public function display()
    {
        $this->engine->display('header.tpl');
        $this->engine->display($this->temp_aux);
        $this->engine->display('registraractuacion.tpl');
        $this->engine->display('footer.tpl');
    }
    
    public function run()
    {
        try {if (isset($this->get->option)){$this->{$this->get->option}();}}
        catch (Exception $e) 
		{
			$this->error=1; $this->msg_warning=$e->getMessage();
			$this->engine->assign('type_warning',$this->type_warning);
			$this->engine->assign('msg_warning',$this->msg_warning);
			$this->temp_aux = 'message.tpl';
		}    
        $this->display();
    }
}

$call = new c_registraractuacion();
$call->run();

?>
