<?php

//trae todas las fincas y todos los granjeros, luego mediante 2 foreach los recorre y busca el maximo de gallinas y el maximo de vacas,  luego extrae el nombre del granjero, el id y la cantidad de vacas o gallinas maximas

require('configs/include.php');

class c_listarelenco extends super_controller {
    
    public function display()
    {
        $this->engine->display('header.tpl');
        $this->engine->display($this->temp_aux);
        $this->engine->display('listarelenco.tpl');
        $this->engine->display('footer.tpl');
    }
    
    public function run()
    {
        $options['actor']['lvl2'] = "all";
		$options['pelicula']['lvl2'] = "all";
		$options['actuacion']['lvl2'] = "all";
		$this->orm->connect();
        $this->orm->read_data(array("actor"), $options);
		$actor = $this->orm->get_objects("actor");
		$this->orm->read_data(array("pelicula"), $options);
		$pelicula = $this->orm->get_objects("pelicula");
		$this->orm->read_data(array("actuacion"), $options);
		$actuacion = $this->orm->get_objects("actuacion");
        $this->orm->close();
		
		$this->engine->assign('actor', $actor);
		$this->engine->assign('pelicula', $pelicula);
		$this->engine->assign('actuacion', $actuacion);
		 
        $this->display();
    }
}

$call = new c_listarelenco();
$call->run();

?>
