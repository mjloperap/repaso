<?php

class pelicula extends object_standard{
	//atribute variables
	protected $codigo;
	protected $nombre;
	protected $genero;
	protected $puntaje;
	protected $idioma;
		
	//components
	var $components = array();
	
	//auxiliars for primary key and for files
	var $auxiliars = array();
	
	//data about the atributes
	public function metadata(){
		return array("codigo" => array(), "nombre" => array(), "genero" => array(), "puntaje" => array(), "idioma" => array());
	}
	
	public function primary_key(){
		return array("codigo");
	}
	
	public function relational_keys($class, $rel_name){
		switch($class){
			default:
			break;
		}
	}
	
}

?>