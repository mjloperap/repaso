<?php

class actor extends object_standard{
	//atribute variables
	protected $cedula;
	protected $nombre;
	protected $apellido;
		
	//components
	var $components = array();
	
	//auxiliars for primary key and for files
	var $auxiliars = array();
	
	//data about the atributes
	public function metadata(){
		return array("cedula" => array(), "nombre" => array(), "apellido" => array());
	}
	
	public function primary_key(){
		return array("cedula");
	}
	
	public function relational_keys($class, $rel_name){
		switch($class){
			default:
			break;
		}
	}
	
}

?>