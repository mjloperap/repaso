<?php

class actuacion extends object_standard{
	//atribute variables
	protected $id;
	protected $nombrepersonaje;
	protected $pelicula;
	protected $actor;
			
	//components
	var $components = array();
	
	//auxiliars for primary key and for files
	var $auxiliars = array();
	
	//data about the atributes
	public function metadata(){
		return array(
		"id" => array(), 
		"nombre" => array(), 
		"pelicula" => array("foreign_name" => "a_p", "foreign" => "pelicula", "foreign_attribute" => "codigo"),
		"actor" => array("foreign_name" => "a_a", "foreign" => "actor", "foreign_attribute" => "cedula")
		);
	}
	
	public function primary_key(){
		return array("id");
	}
	
	public function relational_keys($class, $rel_name){
		switch($class){
			case "pelicula":
				switch($rel_name){
					case "a_p":
					return array("pelicula");
					break;
				}
			break;
			
			case "actor":
				switch($rel_name){
					case "a_a":
					return array("actor");
					break;
				}
			break;
			
			default:
		break;
		}
	}

}
	
?>