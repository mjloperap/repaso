<?php

require('configs/include.php');

class c_eliminar_actuacion extends super_controller {
    
    public function delete()
    {
        $actuacion = new actuacion($this->post);
        
		if(is_empty($actuacion->get('actor'))){
			throw_exception("Debe ingresar una cedula");
		}
		
		else{
			
		$options['actor']['lvl2'] = "by_cedula";
		$this->orm->connect();
		$this->orm->read_data(array("actor"), $options);
		$actor = $this->orm->get_objects("actor");
		$this->orm->close();
		
		
		$this->orm->connect();
        $this->orm->delete_data("by_actor",$actuacion);
        $this->orm->close();
        
        
		$this->type_warning = "success";
        $this->msg_warning = "Actuacion eliminada correctamente";
        
        $this->temp_aux = 'message.tpl';
        $this->engine->assign('type_warning',$this->type_warning);
        $this->engine->assign('msg_warning',$this->msg_warning);
			
		}
    }

    public function display()
    {
        $this->engine->display('header.tpl');
        $this->engine->display($this->temp_aux);
        $this->engine->display('eliminar_actuacion.tpl');
        $this->engine->display('footer.tpl');
    }
    
    public function run()
    {
        try {if (isset($this->get->option)){$this->{$this->get->option}();}}
        catch (Exception $e) 
		{
			$this->error=1; $this->msg_warning=$e->getMessage();
			$this->engine->assign('type_warning',$this->type_warning);
			$this->engine->assign('msg_warning',$this->msg_warning);
			$this->temp_aux = 'message.tpl';
		}    
        $this->display();
    }
}

$call = new c_eliminar_actuacion();
$call->run();

?>
