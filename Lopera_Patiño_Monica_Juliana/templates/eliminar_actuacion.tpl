<div class="square">
	
{if isset($actuacion)}

	<table width="100%" border="0" cellpadding="0" cellspacing="5">
		<tr><td><b>DATOS DEL ACTOR SELECCIONADO</b></td></tr>

		<tr><td>
 
			<b>Cedula del actor:</b> {$actor->get('Cedula')}<br />
			<b>Nombre del actor:</b> {$actor->get('nombre')}<br />
			<b>Apellido del actor:</b> {$actor->get('apellido')}<br />
			
		</td></tr>
		

		<tr><td><b>DATOS DE LAS ACTUACIONES DEL ACTOR</b></td></tr>
		
		<table>

				<thead>
				<tr>
					<th>Pelicula:</th>
					<th>Nombre del personaje:</th>
				</tr>
				</thead>

				<body>
					{section loop=$actuacion name=i}
					<tr>
						<td><center>{$actuacion[i]->get('pelicula')}</center></td>
						<td><center>{$actuacion[i]->get('nombre')}</center></td>
					</tr>
					{/section}
				</body>

		</table>
		
 
	</table>


{else}

	<form action="{$gvar.l_global}eliminar_actuacion.php?option=delete" method="post">
		<table width="100%" border="0" cellpadding="0" cellspacing="5">
			<tr><td>
			<b>Eliminar una actuacion</b><br /><br />

			<b>Ingrese un id del actor a quien le elimanara la actuacion:</b> <input type="text" name="actor" /><br />

			<input class="btn btn-primary" type="submit" value="Mostar Actuaciones" />
			</td></tr>
		</table>
	</form>
	
{/if}

</div>
 