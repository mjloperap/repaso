<?php

class actuacion extends object_standard{
	//atribute variables
	protected $actor;
	protected $pelicula;
	protected $personaje;
	
	//components
	var $components = array();
	
	//auxiliars for primary key and for files
	var $auxiliars = array();
	
	//data about the atributes
	public function metadata(){
		return array("personaje" => array(),"actor" => array("foreign_name" => "a_a", "foreign" => "actor", "foreign_attribute" => "cedula"), "pelicula" => array("foreign_name" => "a_p", "foreign" => "pelicula", "foreign_attribute" => "codigo"));
	}
	
	public function primary_key(){
		return array("actor", "pelicula", "personaje");
	}
	
	public function relational_keys($class, $rel_name){
		switch($class){
		
			case "actor":
			switch($rel_name){
				case "a_a":
				return array("actor");
				break;
			}
			break;
			
			case "pelicula":
			switch($rel_name){
				case "a_p":
				return array("pelicula");
				break;
			}
			break;
			
			default:
			break;
		}
	}
}

?>